**Integrasi SSO BSSN**
===================================

**Requirement**
1. Menggunakan jaringan internal BSSN / VPN
2. Menggunakan HTTPS, jika di localhost bisa menggunakan self sign
3. Menggunakan url: 127.0.0.1 / localhost/ localhost (selain url tsb silahkan hub admin untuk penambahan servis)


**Installasi**

`composer require sso-bssn/sso-bssn`

Buat direktori SSO di App. Sehingga direktori akan terlihat seperti `App\SSO`.
Copy file konfigurasi SSO  `SSO.php` yang ada di dalam Example ke direktori `App\SSO`

Cara penggunaan di laravel:
```
use App\Sso;
```

```
Sso::authenticate(); // code ini akan mengarahkan web browser ke halaman login BSSN
$user = Sso::getUser(); //code ini akan mengambil data user yang ada di dalam atribut SSO
```

Contoh ketika Sso::authenticate() dieksekusi aplikasi akan di redirect ke halaman
> sso.bssn.go.id/cas/login?service=https%3A%2F%2Fcoba.bssn.go.id%2Fsso%2Fcallback


Nb: akun untuk melakukan testing dapat diminta ke admin
